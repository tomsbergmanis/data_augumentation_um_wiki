#!/bin/bash
#./wiki_extractor_base.sh Estonian data/Estonian
lang="$1"
data="$2"
base_dir=/disk/scratch/s1044253
moses_scripts_dir=$base_dir/mosesdecoder/scripts
# tokenize sentences, remove tags, tokenize
python3 ../wikiextractor/WikiExtractor.py $data/$lang.bz2  --no-templates -o - | perl $moses_scripts_dir/ems/support/split-sentences.perl -b -l $lang  | sed -E -e '/<P>|<\/doc>|<doc id="[^"]+" url="[^"]+" title="[^"]+">/d' |  perl $moses_scripts_dir/tokenizer/tokenizer.perl -l $lang -threads 10 -no-escape >  $data/$lang.sent.tok &&
#shuffle 
cat $data/$lang.sent.tok | awk 'BEGIN{srand();} {printf "%06d %s\n", rand()*1000000, $0;}' | sort -n | cut -c8- >  $data/$lang.sent.tok.shuffled 

