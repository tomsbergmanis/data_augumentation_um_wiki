#!/usr/bin/python3

__author__ = "Toms Bergmanis"
__email__ = "Toms.Bergmanis@gmail.com"

import os

def file_exists(path, msg):
    path = os.path.abspath(path)
    if not os.path.exists(path):
        print(msg)
        exit(-1)
    else:
        return True


def make_dir(path):
    #print(path)
    #path = os.path.abspath(path)
    #print(path)
    directory = os.path.dirname(path)
    print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)
