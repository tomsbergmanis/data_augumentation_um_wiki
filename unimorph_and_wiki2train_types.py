#!/usr/bin/python3

__author__ = "Toms Bergmanis"
__email__ = "Toms.Bergmanis@gmail.com"

import sys
from collections import defaultdict
import utils

#Unimoprh inflection tables
unimorph_file = sys.argv[1]
utils.file_exists(unimorph_file, "{} file does not exist".format("Unimorph"))

#Monolingual text, 1 sentence per line, tokenized sentences
text_file = sys.argv[2]
utils.file_exists(text_file, "{} file does not exist".format("Monoligual"))

output_file_name = sys.argv[3]
utils.make_dir(output_file_name)

# sentence context. We used 0, and 20 ch
n = int(sys.argv[4]) 

#max number of training examples to select
try:
    NO_LINES = int(sys.argv[5])
except IndexError:
    NO_LINES = 10000
    
try: #ignore list 
    ignore = sys.argv[6]
    ignore = set(open(ignore, "r").read().split("\n"))

except:
    ignore = set([])

SPACE = "</s>"
LC = '<lc>'
RC = '<rc>'
WBEGIN = '<w>'
WEND = '</w>'

# read unimorph inflection tables
inflection2lemma = defaultdict(list)
with open(unimorph_file, "r") as ufh:
    for line in ufh:
        try:
            lemma, inflection, _ = line.split()
        except:
            continue
        inflection2lemma[inflection].append(lemma)

# prune ambiguous inflections
inflections = set(inflection2lemma.keys())
for inflection in inflections:
    if len(inflection2lemma[inflection]) > 1:
        del inflection2lemma[inflection]

# prune multi-part inflections
for inflection in inflections:
    parts = inflection.split()
    if len(parts) > 1:
        del inflection2lemma[inflection]

output = []
count = 0
selected_types = defaultdict(list)
with open(text_file, "r") as tfh:

    for line in tfh:        
        line_contents = line.strip().split()
        for i, inflection in enumerate(line_contents):
            if inflection in inflection2lemma and len(set(inflection2lemma[inflection])) == 1:
                if len(selected_types[inflection]) <= NUMBER_OF_EXAMPLES_PER_TYPE:
                    selected_types[inflection].append((" ".join(line_contents[:i]), " ".join(line_contents[i+1:])))

selected_inflections = selected_types.keys()
print(len([i for i, v in selected_types.items() if len(v) >= NUMBER_OF_EXAMPLES_PER_TYPE]))
count = NO_LINES
while count > 0 :
    for inflection in selected_inflections:
        if len(selected_types[inflection]) == 0 or inflection in ignore:
            continue
        count -= 1
        left_context, right_context = selected_types[inflection][-1]
        del selected_types[inflection][-1]
        if len(selected_types[inflection]) == []:
           del selected_types[inflection]
        lemma = " ".join([letter for letter in inflection2lemma[inflection][0]])
        inflection = " ".join([letter for letter in inflection])
        if n == 0:
            output.append((lemma, " ".join([LC, inflection, RC])))
            if count == 0:
                break
            continue

        left_context = left_context[-min(n, len(left_context)):]
        left_context = " ".join([SPACE if letter == " " else letter for letter in left_context])
        right_context = right_context[:min(n, len(right_context))]
        right_context = " ".join([SPACE if letter == " " else letter for letter in right_context])
        output.append((lemma, " ".join([left_context, LC, inflection, RC, right_context])))
        if count == 0 or len(selected_types.keys()) == 0:
            break


with open(output_file_name+"-targets", "w") as tfh:
    with open(output_file_name + "-sources", "w") as sfh:
        for target, source in output:
            tfh.write("{} {} {}\n".format(WBEGIN, target, WEND))
            sfh.write("{} {} {}\n".format(WBEGIN, source, WEND))

