# Data Augmentation for Context-Sensitive Neural Lemmatization Using Inflection Tables and Raw Text
[Toms Bergmanis](http://homepages.inf.ed.ac.uk/s1044253/) and [Sharon Goldwater](https://homepages.inf.ed.ac.uk/sgwater/) _In Proceedings of the Conference of the North American Chapter of the Association for Computational Linguistics: Human Language Technologies 2019_ 

## Abstract
_Lemmatization aims to reduce the sparse data problem by relating the inflected forms of a word to its dictionary form. Using context can help, both for unseen and ambiguous words. Yet most context-sensitive approaches require full lemma-annotated sentences for training, which may be scarce or unavailable in low-resource languages. In addition (as shown here), in a low-resource setting a lemmatizer can learn more from n labeled examples of distinct words (types) than from n (contiguous) labeled tokens, since the latter contain far fewer distinct types. To combine the efficiency of type-based learning with the benefits of context, we propose a way to train a context-sensitive lemmatizer with little or no labeled corpus data, using inflection tables from the [UniMorph](http://unimorph.org/) project and raw text examples from Wikipedia that provide sentence contexts for the unambiguous UniMorph examples. Despite these being unambiguous examples, the model successfully generalizes from them, leading to improved results (both overall, and especially on unseen words) in comparison to a baseline that does not use context._

## Data Preparation:
* [wiki_extractor_base.sh](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/wiki_extractor_base.sh) script for processing Wiki Dumps. Requires [WikiExtractor](https://github.com/attardi/wikiextractor) and scripts from [Moses statistical machine translation system](https://github.com/moses-smt/mosesdecoder)
* [unimorph_and_wiki2train_types.py](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/unimorph_and_wiki2train_types.py) produces data for augmentation that can be combined with other data e.g. training data from [UDT](https://github.com/UniversalDependencies/) 
* [extract_wordlist_from_lematus_training_data.py](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/extract_wordlist_from_lematus_training_data.py) script for preparing lists of forms to be ignored by *unimorph_and_wiki2train_types.py*

## Data Stats:
* [analyse_training_sets.py](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/analyse_training_sets.py) a script to draw a Venn diagram for UDT train, UM train and UDT dev inflection overlap
* [udt_unimorph_diff.py](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/udt_unimorph_diff.py) script to find the number of potentially conflicting lemmas

## Experiments:
In our experiments we used:
 [Lematus](https://bitbucket.org/tomsbergmanis/lematus/),
 [Lemming](http://cistern.cis.lmu.de/lemming/),
 [Hard monotonic attention model for neural transition based morphology](https://github.com/ZurichNLP/coling2018-neural-transition-based-morphology),

Datasets from data augmentation experiments:
 [1k UDT types](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/data/1K-UDT-types/),
 [1k UDT types + 1k UM types](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/data/1K-UDT-types-1K-UM-types/),
 [1k UDT types + 5k UM types](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/data/1K-UDT-types-5K-UM-types/),
 [1k UDT types + 10k UM types](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/data/1K-UDT-types-10K-UM-types/),

## Results:
* [permutation_test.py](https://bitbucket.org/tomsbergmanis/data_augumentation_um_wiki/src/master/permutation_test.py) script for permutation significance test

