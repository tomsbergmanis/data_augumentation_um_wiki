#!/usr/bin/python3

__author__ = "Toms Bergmanis"
__email__ = "Toms.Bergmanis@gmail.com"

import sys
import re
from collections import defaultdict

udt_dict = defaultdict(list)
um_dict = defaultdict(list)
udt_re = re.compile("[1-90]+\t([\w|-]+)\t([\w|-]+)\t([\w]+)")

with open(sys.argv[1], "r") as udt:
    for line in udt:
        a = udt_re.search(line)
        if a:
            udt_dict[a.group(1)].append(a.group(2))
            

with open(sys.argv[2], "r") as um:
    for line in um:
        try:
            a, b, _ = line.split("\t")
            um_dict[b].append(a)
        except:
            pass

for w, l in udt_dict.items():
    if len(l) == 1:
        l = l[0]
        if um_dict[w]:
            if len(um_dict[w]) == 1:
                ul = um_dict[w][0]
                if ul != l:
                    print("UDT Lemma {}, UNIMOPRH LEMMA {}, word {}".format(l, ul, w))

print(len(set(um_dict.keys()) & set(udt_dict.keys())) / len(set(udt_dict.keys())))

