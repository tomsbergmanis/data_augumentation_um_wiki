#!/usr/bin/python3

__author__ = "Toms Bergmanis"
__email__ = "Toms.Bergmanis@gmail.com"

import sys
from collections import Counter, defaultdict
import matplotlib.pyplot as plt
from matplotlib_venn import venn3


def read_file_pair(path, type="train"):
    inflection2lemmas = defaultdict(list)
    if type == "hypothesis":
        file_1 = "{}/dev-sources".format(path)
        file_2 = "{}/dev-hypothesis".format(path)
    else:
        file_1 = "{}/{}-sources".format(path, type)
        file_2 = "{}/{}-targets".format(path, type)

    with open(file_1, "r") as fh1:
        with open(file_2, "r") as fh2:
            for line in fh1:
                inflection = get_inflection(line)
                lemma = get_lemma(fh2.readline())
                inflection2lemmas[inflection].append(lemma)
    return inflection2lemmas

def get_inflection(line):
    try:
        return "".join(line.split("<lc>")[1].split("<rc>")[0].strip().split()).lower()
    except:
        return "".join(line.split("<w>")[1].split("</w>")[0].strip().split()).lower()

def get_lemma(line):
    return "".join([l for l in line.strip().split()[1:-1] if l != "#"]).lower()

def get_overlapping_subsets(A, B, C):
    s1 = A - B - C
    s2 = B - A - C
    s3 = A & B - C
    s4 = C - A - B
    s5 = A & C - B
    s6 = B & C - A
    s7 = A & B & C
    return (s1, s2, s3, s4, s5, s6, s7)


path2UM = sys.argv[1]
path2UD = sys.argv[2]
lang = sys.argv[3]
UM_1k_inflections2lemmas = read_file_pair(path2UM)
UD_1k_inflections2lemmas = read_file_pair(path2UD)
DEV_inflections2lemmas = read_file_pair(path2UD, "dev")

s1, s2, s3, s4, s5, s6, s7 = get_overlapping_subsets(set(UM_1k_inflections2lemmas.keys()), set(UD_1k_inflections2lemmas.keys()),
                           set(DEV_inflections2lemmas.keys()))
fig = plt.figure()
venn3(subsets = [len(s) for s in [s1, s2, s3, s4, s5, s6, s7]], set_labels = ('UniMorph-1K-Types', 'UDT-1K-Types', 'UDT-DEV'))

plt.savefig('{}'.format(lang), bbox_inches='tight')
