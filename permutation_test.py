#!/usr/bin/python3

__author__ = "Toms Bergmanis"
__email__ = "Toms.Bergmanis@gmail.com"

"""Monte Carlo method:  for each comparison we generated 10000 random samples, where each sample randomly swapped the two systems’ results
for each language with probability .5. We then obtaine a p-value by computing the proportion of samples for which the difference in average
results was at least as large as the difference observed in our experiments."""

from random import random
import sys

N = 100000
def permute(A, B):
    A_new = []
    B_new = []
    for a, b in zip(A, B):
        if random() > 0.5:
            A_new.append(b)
            B_new.append(a)
        else:
            A_new.append(a)
            B_new.append(b)
    return A_new, B_new


def get_diff(A,B):
    A_mean = sum(A) / len(A)
    B_mean = sum(B) / len(B)
    return A_mean - B_mean


data = []
#assumes input file to have L lines, where first L/2 contain scores for System A, and the last L/2 scores for System B
with open(sys.argv[1], "r") as f:
    for line in f:
        data.append(float(line.strip()))


A = data[0:int(len(data)/2)]
B = data[int(len(data)/2):]

samples = []
for i in range(0,N):
    A_new, B_new = permute(A, B)
    samples.append(get_diff(A_new,B_new))

samples.sort()
t = get_diff(A,B)

i = 0
print("difference:",t)
for sample in samples:
    if sample > t:
        i += 1
print("p-value", float(i) / N)


