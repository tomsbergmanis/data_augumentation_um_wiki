#!/usr/bin/python3

__author__ = "Toms Bergmanis"
__email__ = "Toms.Bergmanis@gmail.com"
import sys
for line in sys.stdin:
    try:
        print("".join(line.split("<lc>")[1].split("<rc>")[0].strip().split()).lower())
    except:
        print("".join(line.split("<w>")[1].split("</w>")[0].strip().split()).lower())
